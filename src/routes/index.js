"use strict";

const express = require("express");
const userController = require("../controller/user.controller");

const router = express.Router();

router.use("/auth", require("./auth"));
router.use("/blogs", require("./blogs"));

module.exports = router;
