"use strict";

const express = require("express");
const asyncHandler = require("../../helpers/asyncHandler");
const BlogController = require("../../controller/blog.controller");
const { Authentication } = require("../../auth/checkAuth");

const router = express.Router();

router.use(Authentication);

router.get("/", asyncHandler(BlogController.getAllCommentByUser));
router.post("/new", asyncHandler(BlogController.newComment));
router.put("/update", asyncHandler(BlogController.updateComment));
router.delete("/delete", asyncHandler(BlogController.deleteCommentByUser));

module.exports = router;
