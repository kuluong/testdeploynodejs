"use strict";
const express = require("express");
const asyncHandler = require("../../helpers/asyncHandler");
const userController = require("../../controller/user.controller");
const { Authentication } = require("../../auth/checkAuth");

const router = express.Router();

router.post("/register", asyncHandler(userController.register));
router.post("/login", asyncHandler(userController.login));

router.use(Authentication);

router.get("/demo", (req, res, next) => {
  res.send("demo");
});

module.exports = router;
