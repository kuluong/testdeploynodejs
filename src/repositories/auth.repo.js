"use strict";

const userModel = require("../modules/user.model");

const findUserByUsername = async (username) => {
  return userModel.findOne({ username }).lean();
};
module.exports = {
  findUserByUsername,
};
