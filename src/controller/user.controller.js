"use strict";

const userService = require("../services/user.service");

class userController {
  login = async (req, res, next) => {
    return res.status(200).json({
      data: await userService.login(req.body),
    });
  };
  register = async (req, res, next) => {
    return res.status(200).json({
      data: await userService.register(req.body),
    });
  };
  demo = async (req, res, next) => {
    return res.status(200).json({
      data: {},
    });
  };
}
module.exports = new userController();
