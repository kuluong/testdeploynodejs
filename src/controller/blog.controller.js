"use strict";

const BlogService = require("../services/blogs.service");

class BlogController {
  static getAllCommentByUser = async (req, res, next) => {
    return res.status(200).send({
      status: "success",
      data: await BlogService.getAllCommentByUser(req.user.username),
    });
  };
  static newComment = async (req, res, next) => {
    return res.status(201).send({
      status: "success",
      data: await BlogService.newComment({
        ...req.body,
        username: req.user.username,
      }),
    });
  };
  static updateComment = async (req, res, next) => {
    return res.status(200).send({
      status: "success",
      data: await BlogService.updateComment({
        ...req.body,
        username: req.user.username,
        idComment: req.body.id,
      }),
    });
  };
  static deleteCommentByUser = async (req, res, next) => {
    return res.status(200).send({
      status: "success",
      data: await BlogService.deleteCommentByUser({
        username: req.body.username,
        idComment: req.body.id,
      }),
    });
  };
}

module.exports = BlogController;
