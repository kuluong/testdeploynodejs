const compression = require("compression");
const express = require("express");
const { default: helmet } = require("helmet");
const morgan = require("morgan");
require("dotenv").config();
const app = express();

//init middleWare
app.use(morgan("dev"));
app.use(helmet());
app.use(compression());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//connect db
require("./database/init.mongodb");

//Routes
app.use("/v1/api/", require("./routes"));
app.get("/", function (req, res, next) {
  return res.send("hello world");
});
// handling Error
app.use((req, res, next) => {
  const error = new Error("Not Found");
  error.status = 404;
  next(error);
});
app.use((err, req, res, next) => {
  const statusCode = err.status || 500;
  return res.status(statusCode).json({
    status: "error",
    code: statusCode,
    // stack: err.stack,
    message: err.message || "Internal Server Error",
  });
});

module.exports = app;
