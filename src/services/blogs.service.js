"use strict";

const blogModel = require("../modules/blog.model");
const ErrorResponse = require("../responPhrase/errorResponse");
const { getInfoData } = require("../utils/getInfoData");

class BlogService {
  static getAllCommentByUser = async (username) => {
    if (!username) {
      throw new ErrorResponse("user not exists!!", 400);
    }

    const foundCommentByUser = await blogModel
      .find({ username })
      .select(["username", "content"])
      .lean();

    if (!foundCommentByUser) {
      throw new ErrorResponse("comment not found by user", 404);
    }

    return foundCommentByUser;
  };
  static newComment = async (payload) => {
    const { username, content } = payload;
    if (!username) {
      throw new ErrorResponse("Forbidden Error", 403);
    }
    console.log(username);
    const createComment = await blogModel.create({
      username,
      content,
    });
    return getInfoData({ fields: ["username", "content"], object: createComment });
  };
  static updateComment = async (payload) => {
    const { username, content, idComment } = payload;
    if (!username || !idComment) {
      throw new ErrorResponse("Forbidden Error", 403);
    }
    const findComment = await blogModel.findOne({ _id: idComment }).lean();

    if (findComment.username !== username) {
      throw new ErrorResponse(
        `you can't update this comment, because you are not the owner of the comment `
      );
    }

    const update = await blogModel.findOneAndUpdate(
      { username, _id: idComment },
      payload,
      {
        new: true,
        upsert: true,
      }
    );
    if (!update) {
      throw new ErrorResponse("update fail", 400);
    }
    return getInfoData({ fields: ["username", "content"], object: update });
  };
  static deleteCommentByUser = async ({ username, idComment }) => {
    if (!username || !idComment) {
      throw new ErrorResponse("Forbidden Error", 403);
    }
    const findComment = await blogModel.findOne({ _id: idComment }).lean();
    if (!findComment) {
      throw new ErrorResponse("comment has deleted or not exists ", 400);
    }
    if (findComment.username !== username) {
      throw new ErrorResponse(
        `you can't update this comment, because you are not the owner of the comment `
      );
    }

    await blogModel.findByIdAndDelete(idComment);
    return null;
  };
}

module.exports = BlogService;
