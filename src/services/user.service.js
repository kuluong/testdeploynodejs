"use strict";

const bcrypt = require("bcrypt");
const authUtil = require("../auth/authUtil");
const { findUserByUsername } = require("../repositories/auth.repo");
const ErrorResponse = require("../responPhrase/errorResponse");
const userModel = require("../modules/user.model");
class userService {
  static login = async (payload) => {
    const { username, password } = payload;

    if (!username) throw new ErrorResponse("username not empty", 403);
    if (!password) throw new ErrorResponse("password not empty", 403);

    const foundUser = await findUserByUsername(username);
    if (!foundUser) throw new ErrorResponse("username not registered!", 401);
    //match password
    const match = await bcrypt.compare(password.toString(), foundUser.password);
    if (!match) throw new ErrorResponse("Authentication error", 401);

    const token = await authUtil.createTokenPair(
      { username: foundUser.username },
      process.env.KEY_SECRET
    );
    console.log(token)
    return {
      data: {
        username: foundUser.username,
      },
      token,
    };
  };

  static register = async (payload) => {
    const { username, password } = payload;

    if (!username) {
      throw new ErrorResponse("username not empty!", 404);
    }
    const foundAccount = await findUserByUsername(username);
    if (foundAccount) {
      throw new ErrorResponse("username exists!", 403);
    }
    const passwordHash = await bcrypt.hash(password, 10);
    const createUser = await userModel.create({
      username,
      password: passwordHash,
    });
    if (!createUser) throw new ErrorResponse("create user fail", 403);
    //tao token va luu vao db
    const token = await authUtil.createTokenPair({ username }, process.env.KEY_SECRET);
    console.log(token);
    return {
      account: {
        username: createUser.username,
      },
      token: token,
    };
  };
}

module.exports = userService;
