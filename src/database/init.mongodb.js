"use strict";
const mongoose = require("mongoose");
const connStr = `mongodb+srv://luongtrandev:${process.env.PASSWORD_DB}@nodejs-example.dthiwul.mongodb.net/`;

class Database {
  constructor() {
    this.connect();
  }
  connect(type = "mongodb") {
    mongoose
      .connect(connStr)
      .then((res) => {
        console.log("connect to DB success");
      })
      .catch((err) => {
        console.log(`connect to db fail ${err}`);
      });
  }

  static getInstance() {
    // kiem tra db da ket noi chua, chua thi khoi tao, con da ket noi roi thi van duy tri ket noi do
    if (!Database.instance) {
      Database.instance = new Database();
    }
    return Database.instance;
  }
}

module.exports = Database.getInstance();
