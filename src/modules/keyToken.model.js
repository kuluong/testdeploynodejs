const mongoose = require("mongoose");

const DOCUMENT_NAME = "token";
const COLLECTION_NAME = "tokens";

const keyTokenSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      ref: "user",
    },
    token: {
      type: String,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

module.exports = new mongoose.model(DOCUMENT_NAME, keyTokenSchema);
