"use strict";
const mongoose = require("mongoose");

const DOCUMENT_NAME = "blog";
const COLLECTION_NAME = "blogs";

const blogSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      require: true,
      ref: "user",
    },
    content: {
      type: String,
      require: true,
    },
    publish: {
      type: Boolean,
      default: true,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

module.exports = mongoose.model(DOCUMENT_NAME, blogSchema);
