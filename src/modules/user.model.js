const mongoose = require("mongoose");

const DOCUMENT_NAME = "user";
const COLLECTION_NAME = "users";

const userSchema = new mongoose.Schema(
  {
    username: {
      type: String,
      require: "true",
    },
    password: {
      type: String,
      require: true,
    },
  },
  {
    timestamps: true,
    collection: COLLECTION_NAME,
  }
);

module.exports = mongoose.model(DOCUMENT_NAME, userSchema);
