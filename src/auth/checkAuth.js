"use strict";

const asyncHandler = require("../helpers/asyncHandler");
const ErrorResponse = require("../responPhrase/errorResponse");
const authUtil = require("./authUtil");

const Authentication = asyncHandler(async (req, res, next) => {
  /**
   * check token exists
   * check token valid
   */
  const headerAccessToken = req.headers.authorization;

  if (!headerAccessToken || !headerAccessToken.startsWith("Bearer")) {
    throw new ErrorResponse("login fail,please login again!", 401);
  }
  const token = headerAccessToken.split(" ")[1];

  if (!token) {
    throw new ErrorResponse("login fail,please login again!", 401);
  }

  //verify token

  const decode = await authUtil.verifyToken(token, process.env.KEY_SECRET);
  if (decode) {
    req.user = decode;
  }
  next();
});

module.exports = {
  Authentication,
};
