"use strict";
const JWT = require("jsonwebtoken");

const authUtil = {
  createTokenPair: async (payload, keySecret) => {
    const token = JWT.sign(payload, keySecret, {
      expiresIn: 60 * 60,
    });
    JWT.verify(token, keySecret, (err, decode) => {
      if (err) {
        console.log(`error verify`, err);
      } else {
        console.log(`decode verify`, decode);
      }
    });
    return token;
  },
  verifyToken: async (token, keySecret) => {
    const verifyToken = JWT.verify(token, keySecret);
    return verifyToken;
  },
};

module.exports = authUtil;
