const express = require("express");
const app = require("./src/app");

app.use("/", require("./routes"));
app.listen(3024, (req, res, next) => {
  console.log("server running success");
});
